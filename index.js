let topNavBar = document.querySelector('.topNavBar')
let logo = document.querySelector('.logo')

window.addEventListener('scroll', function() {
	topNavBar.classList.toggle('sticky', window.scrollY > 0)
	logo.classList.toggle('sticky', window.scrollY > 0)
})




//navbar collapse
const menuBtn = document.querySelector('.menu-btn');
let menuOpen = false;
menuBtn.addEventListener('click', () => {
    if(!menuOpen) {
        menuBtn.classList.add('open');
        menuOpen = true;
    } else { 
        menuBtn.classList.remove('open');
        menuOpen = false;
    }
});


//Navbar smallphones
const toggleButton = document.querySelector('.menu-btn')
const navBarNav = document.querySelector('.navbar-nav')
const body = document.querySelector('body')

toggleButton.addEventListener('click', () => {
	navBarNav.classList.toggle('active');
	body.classList.toggle('active');
})